const bodyParser = require('body-parser');
const express = require('express');
const ejs = require('ejs');
const config = require('./config');

const server = express();

const { server: { port, host } } = config;

server.engine('html', ejs.renderFile);
server.listen(port, host, () => console.info(`Server started and listen port: ${port}`));
server.set('view engine', 'html');
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

module.exports = server;
