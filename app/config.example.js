exports.server = {
  port: 5000,
  host: 'localhost',
};

exports.mongodb = {
  port: '27017',
  host: 'localhost',
  name: 'test',
  options: {
    useNewUrlParser: true,
  },
};
