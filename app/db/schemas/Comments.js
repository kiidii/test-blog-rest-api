const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  author: { type: String, required: true },
  articleId: { type: String, required: true },
  text: { type: String, required: true },
  parentId: { type: String, required: false },
  created: { type: Date, default: Date.now },
  status: { type: String, default: 'new' },
});

module.exports = mongoose.model('Comments', Schema);
