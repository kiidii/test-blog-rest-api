const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
  label: { type: String, required: false },
  title: { type: String, required: true },
  metaTitle: { type: String, required: true },
  metaDescription: { type: String, required: true },
  metaKeywords: { type: String, required: true },
  body: { type: String, required: true },
  author: { type: String, required: false },
  authorName: { type: String, required: false },
  created: { type: Date, default: Date.now },
  status: { type: String, default: 'new' },
});

module.exports = mongoose.model('Blog', Schema);
