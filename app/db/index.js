const mongoose = require('mongoose');
const config = require('../config');

const {
  mongodb: {
    host,
    name,
    options,
    port,
  },
} = config;

mongoose.connect(`mongodb://${host}:${port}/${name}`, options, (error) => {
  if (error) throw console.error(error);
  console.info(`Connected to mongodb://${host}:${port}/${name}`);
});

const db = mongoose.connection;

module.exports = db;
