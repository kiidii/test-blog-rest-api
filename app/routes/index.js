const fs = require('fs');
const server = require('../server');

server.get('/', (req, res) => {
  res.json({
    status: true,
  });
});


fs.readdir(__dirname, (error, files) => {
  if (error) throw console.error(error);
  files.forEach((file) => {
    /* eslint-disable */
    const name = file.split('.')[0];
    const func = require(`./${name}`);
    if (name !== 'index') func();
    /* eslint-enable */
  });
});
