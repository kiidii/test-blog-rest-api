const db = require('../db/index');
const Comment = require('../db/schemas/Comments');
const server = require('../server');

/**
 * FIXME
 * - проблема с обязательными полями
 */

module.exports = () => {
  server.get('/api/comments', async (req, res) => {
    try {
      const where = eval(`(${req.query.where})`);
      const limit = parseInt(where.limit, 10) || 10;
      const offset = parseInt(where.offset, 10) || 0;
      const status = 'active';
      const comments = await db.collection('comments')
        .find(where, { status })
        .skip(offset)
        .limit(limit)
        .toArray();
      res.status(200).json({ comments });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.post('/api/comments', async (req, res) => {
    try {
      console.log(Comment());
      const comment = new Comment(req.body);
      db.collection('comments').insertOne(comment, (error) => {
        if (error) res.status(400).json({ error: error.errors.body.message });
        else res.status(200).json({ comment });
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.get('/comments/:id', async (req, res) => {
    try {
      const { id: _id } = req.params;
      const comment = await db.collection('comments').findOne({ _id });
      if (comment) res.status(200).json(comment);
      else res.status(404).json({ error: 'Comment not found' });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.put('/comments/:id', (req, res) => {
    try {
      const { body, params: { id: _id } } = req;
      db.collection('comments').findOneAndUpdate({ _id }, { $set: body }, (error, { value }) => {
        if (error || value.status === 'delete') res.status(404);
        else res.status(200).json(value);
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.delete('/comments/:id', (req, res) => {
    try {
      const { id: _id } = req.params;
      db.collection('comments').findOneAndUpdate({ _id }, { $set: { status: 'delete' } }, (error, { value }) => {
        if (error || value.status === 'delete') res.status(404).json({ error: 'Comment not found' });
        else res.status(200).json(value);
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });
};
