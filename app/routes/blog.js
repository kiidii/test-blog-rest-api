const db = require('../db/index');
const Blog = require('../db/schemas/Blog');
const server = require('../server');

/**
 * FIXME
 * - исправить objectID и _id
 */

module.exports = () => {
  server.get('/api/blog/count', async (req, res) => {
    try {
      const where = eval(`(${req.query.where})`);
      const limit = parseInt(where.limit, 10) || 10;
      const offset = parseInt(where.offset, 10) || 0;
      const status = 'active';
      const count = await db.collection('blog')
        .find(where, { status })
        .skip(offset)
        .limit(limit)
        .count();
      res.status(200).json({ count });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.post('/api/blog', async (req, res) => {
    try {
      const blog = new Blog(req.body);
      db.collection('blog').insertOne(blog, (error) => {
        if (error) res.status(400).json({ error: error.errors.body.message });
        else res.status(200).json({ blog });
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.get('/blog/:id', async (req, res) => {
    try {
      const { id: _id } = req.params;
      const blog = await db.collection('blog').findOne({ _id });
      if (blog) res.status(200).json(blog);
      else res.status(404).json({ error: 'Blog not found' });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.put('/blog/:id', (req, res) => {
    try {
      const { body, params: { id: _id } } = req;
      db.collection('blog').findOneAndUpdate({ _id }, { $set: body }, (error, { value }) => {
        if (error || value.status === 'delete') res.status(404);
        else res.status(200).json(value);
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });

  server.delete('/blog/:id', (req, res) => {
    try {
      const { id: _id } = req.params;
      db.collection('blog').findOneAndUpdate({ _id }, { $set: { status: 'delete' } }, (error, { value }) => {
        if (error || value.status === 'delete') res.status(404).json({ error: 'Blog not found' });
        else res.status(200).json(value);
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  });
};
