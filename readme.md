# REST API

- [Start Server](#start-server)
    - [Downdload](#downdload-and-run-server)
    - [NPM](#npm)
    - [Run](#run)
- [Blog](#blog)
    - [Get count](#blog-get-count)
    - [Get all](#blog-get-all)
    - [Create](#blog-create)
    - [Read](#blog-read)
    - [Update](#blog-update)
    - [Delete](#blog-delete)

- [Comments](#comments)
    - [Get count](#comments-get-count)
    - [Get all](#comments-get-all)
    - [Create](#comments-create)
    - [Read](#comments-read)
    - [Update](#comments-update)
    - [Delete](#comments-delete)

---

## Start Server


#### Downdload and Run Server

Download the server to your computer using the links below.

[Downdload .zip] (https://gitlab.com/kiidii/test-blog-rest-api/-/archive/master/test-blog-rest-api-master.zip)

[Downdload .tar.gs] (https://gitlab.com/kiidii/test-blog-rest-api/-/archive/master/test-blog-rest-api-master.tar.gz)

[Downdload .tar.bz2] (https://gitlab.com/kiidii/test-blog-rest-api/-/archive/master/test-blog-rest-api-master.tar.bz2)

[Downdload .tar] (https://gitlab.com/kiidii/test-blog-rest-api/-/archive/master/test-blog-rest-api-master.tar)


#### npm

Open the project folder in the terminal and enter the command below
```
npm install
```

#### Run

Open the project folder in the terminal and enter the command below
```
npm run server
```
---

## Blog


#### Blog Get count

URL: GET `/blog/count`

Query params
```
where - JSON string
```

###### Example:

GET
```
http://localhost/api/blog/count?where={"author": "55f6c168b8229d2744d7172b"}
```
Response
```json
{
    count: 1
}
```


#### Blog Get all

URL: GET `/blog`

Response :  Array of relevant records of blog entity

Query params
```
where - JSON string
limit - number (default:10)
offset - number (default: 0)
order - JSON string
```


###### Example:

GET
```
http://localhost/api/blog?where={author: "55f6c168b8229d2744d7172b"}
```
Response: Array of relevant records of blog entity
```json
[
  {
    _id: 586bac1db94b410047881d4d,
    label: 'q2',
    title: 'q2',
    metaTitle: '',
    metaDescription: '',
    metaKeywords: '',
    body: '<p>q2</p>',
    author: '55f6c168b8229d2744d7172b',
    authorName: 'admin',
    created: '03/01/2017 15.50',
    status: 'active',
    __v: 0
  }
]
```


#### Blog Create

URL: POST `/blog`

###### Example:

POST
```
http://localhost/api/blog
```

Body:
```json
  {
   title: 'test article',
   status: 'active',
   metaTitle: 'title',
   metaDescription: 'descsription',
   metaKeywords: 'keywords',
   body: '<p>text text text<br /></p>',
  }
```

Response:
```json
  {
    "__v": 0,
    "label": "test-article-12",
    "title": "test article",
    "metaTitle": "title",
    "metaDescription": "descsription",
    "metaKeywords": "keywords",
    "body": "<p>text text text<br /></p>",
    "_id": "588f6c93acfa0301b30db1e5",
    "authorName": "blog",
    "author": "blog",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "new"
  }
```


#### Blog Read

URL: GET `/blog/:id`

Response :  relevant record of blog entity

###### Example:

GET
```
http://localhost/api/blog/588f647d4d7fb9004bffa53c
```

Response:
```json
{
  "_id": "588f647d4d7fb9004bffa53c",
  "label": "test-article-1",
  "title": "test article",
  "metaTitle": "title",
  "metaDescription": "descsription",
  "metaKeywords": "keywords",
  "body": "<p>text text text<br /></p>",
  "authorName": "blog",
  "author": "blog",
  "created": "2017-01-30T16:34:30.050Z",
  "status": "active",
  "__v": 0
}
```


#### Blog Update

URL: PUT `/blog/:id`

Response : Updated record of blog entity

###### Example:

PUT
```
http://localhost/api/blog/588f6c93acfa0301b30db1e5
```

Body:
```json
  { 
   body: '<p>1111111ascascasc111<br /></p>',
  }
```

Response:
```json
  {
    "_id": "588f6c93acfa0301b30db1e5",
    "label": "test-article-12",
    "title": "test article",
    "metaTitle": "title",
    "metaDescription": "descsription",
    "metaKeywords": "keywords",
    "body": "<p>1111111ascascasc111<br /></p>",
    "__v": 0,
    "authorName": "blog",
    "author": "blog",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "new"
  }
```


#### Blog Delete

URL: DELETE `/blog/:id`

Response : Deleted record of blog entity

###### Example:

DELETE
```
http://localhost/api/blog/588f6c93acfa0301b30db1e5
```

Response
```json
  {
    "_id": "588f6c93acfa0301b30db1e5",
    "label": "test-article-12",
    "title": "test article",
    "metaTitle": "title",
    "metaDescription": "descsription",
    "metaKeywords": "keywords",
    "body": "<p>1111111ascascasc111<br /></p>",
    "__v": 0,
    "authorName": "blog",
    "author": "blog",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "deleted"
  }
```


---

## Comments


#### Comments Get count

URL: GET `/comments/count`

Query params
```
where - JSON string
```

###### Example:

GET
```
http://localhost/api/comments/count?where={articleId: "55f6c168b8229d2744d7172b"}
```
Response
```json
{
    count: 1
}
```


#### Comments Get all

URL: GET `/comments`

Response :  Array of relevant records of comments entity

Query params
```
where - JSON string
limit - number (default:10)
offset - number (default: 0)
order - JSON string
```


###### Example:

GET
```
http://localhost/api/comments?where={"articleId": "55f6c168b8229d2744d7172b"}
```
Response: Array of relevant records of comments entity
```json
[
  {
    "_id": "588f683a02cf7201109cb60f",
    "author": "588f683a02cf7201109cb70f",
    "articleId": "588f647d4d7fb9004bffa53c",
    "text": "Level 1",
    "parentId": "588f67dc02cf7201109cb60c",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  },
  {
    "_id": "588f6a8be5a38601909f922a",
    "author": "588f683a02cf7201109cb70f",
    "articleId": "588f647d4d7fb9004bffa53c",
    "text": "Level 1.2",
    "parentId": "588f67dc02cf7201109cb60c",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  },
  {
    "_id": "588f6cd0acfa0301b30db1e6",
    "author": "588f683a02cf7201109cb70f",
    "articleId": "588f647d4d7fb9004bffa53c",
    "text": "Level 1",
    "parentId": "null",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  }
]
```


#### Comments Create

URL: POST `/comments`

###### Example:

POST
```
http://localhost/api/comments
```

Body:
```json
  {
   "author": "588f683a02cf7201109cb80f",
   "articleId": "588f647d4d7fb9004bffa56c",
   "text": "Level 1",
  }
```

Response:
```json
  {
    "_id": "588f6cd0acfa0301b30db1e6",
    "author": "588f683a02cf7201109cb80f",
    "articleId": "588f647d4d7fb9004bffa56c",
    "text": "Level 1",
    "parentId": "null",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  }
```


#### Comments Read

URL: GET `/comments/:id`

Response :  relevant record of comments entity

###### Example:

GET
```
http://localhost/api/comments/588f6cd0acfa0301b30db1e6
```

Response:
```json
  {
    "_id": "588f6cd0acfa0301b30db1e6",
    "author": "588f683a02cf7201109cb80f",
    "articleId": "588f647d4d7fb9004bffa56c",
    "text": "Level 1",
    "parentId": "null",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  }
```


#### Comments Update

URL: PUT `/comments/:id`

Response : Updated record of comments entity

###### Example:

PUT
```
http://localhost/api/comments/588f6cd0acfa0301b30db1e6
```

Body:
```json
  { 
   text: 'Text Text Text',
  }
```

Response:
```json
  {
    "_id": "588f6cd0acfa0301b30db1e6",
    "author": "588f683a02cf7201109cb80f",
    "articleId": "588f647d4d7fb9004bffa56c",
    "text": "Text Text Text",
    "parentId": "null",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "active",
    "__v": 0
  }
```


#### Comments Delete

URL: DELETE `/comments/:id`

Response : Deleted record of comments entity

###### Example:

DELETE
```
http://localhost/api/comments/588f6cd0acfa0301b30db1e6
```

Response
```json
  {
    "_id": "588f6cd0acfa0301b30db1e6",
    "author": "588f683a02cf7201109cb80f",
    "articleId": "588f647d4d7fb9004bffa56c",
    "text": "Text Text Text",
    "parentId": "null",
    "created": "2017-01-30T16:34:30.050Z",
    "status": "deleted",
    "__v": 0
  }
```